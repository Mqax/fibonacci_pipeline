# -*- coding: utf-8 -*-
"""
Created on Wed Nov  9 13:58:34 2022

@author: maxas
"""


def fibonacci(n):
    a, b = 1, 1
    if n == 1:
        return 1
    else:
        while n >= 1:
            n = n - 1
            c = b
            b = a + b
            a = c
    return b


# changes
